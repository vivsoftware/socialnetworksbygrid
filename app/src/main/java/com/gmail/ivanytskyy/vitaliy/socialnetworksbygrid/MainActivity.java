package com.gmail.ivanytskyy.vitaliy.socialnetworksbygrid;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
public class MainActivity extends AppCompatActivity {
    private int[] mCounter;
    private String[] mSocialNetworkNames;
    private static final String COUNTER_KEY = "couterArray";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSocialNetworkNames = getResources().getStringArray(R.array.social_networks);
        mCounter = new int[mSocialNetworkNames.length];
        GridView gridView = (GridView) findViewById(R.id.gridView);
        CustomBaseAdapter adapter = new CustomBaseAdapter();
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(mCounter[position]++ % 2 == 0){
                    view.findViewById(R.id.itemRootLayout).setBackgroundColor(Color.BLUE);
                }else {
                    view.findViewById(R.id.itemRootLayout).setBackgroundResource(R.drawable.rectangle);
                }
            }
        });
    }
    private class CustomBaseAdapter extends BaseAdapter{
        @Override
        public int getCount() {
            return mSocialNetworkNames.length;
        }
        @Override
        public Object getItem(int position) {
            return mSocialNetworkNames[position];
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = convertView;
            if(view == null){
                view = inflater.inflate(R.layout.item, parent, false);
                String socialNetworkName = mSocialNetworkNames[position];
                Log.d("socialnetworkmytag", socialNetworkName);
                TextView textView = (TextView) view.findViewById(R.id.itemTextView);
                textView.setText(socialNetworkName);
                ImageView imageView = (ImageView) view.findViewById(R.id.itemImageView);
                int socialNetworkIcon = MainActivity.this
                        .getResources()
                        .getIdentifier(
                                socialNetworkName.toLowerCase(),
                                "drawable",
                                getApplicationContext().getPackageName());
                Log.d("socialnetworkmytag", socialNetworkIcon + "");
                if (socialNetworkIcon != 0){
                    imageView.setImageResource(socialNetworkIcon);
                }
            }
            return view;
        }
    }
}
